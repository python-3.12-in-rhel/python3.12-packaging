%global __python3 /usr/bin/python3.12
%global python3_pkgversion 3.12

%global pypi_name packaging

# Tests are disabled in RHEL because we don't have python-pretend.
# Specify --with tests to enable them.
%bcond_with tests

Name:           python%{python3_pkgversion}-%{pypi_name}
Version:        23.2
Release:        1%{?dist}
Summary:        Core utilities for Python packages

License:        BSD-2-Clause OR Apache-2.0
URL:            https://github.com/pypa/packaging
Source0:        %{url}/archive/%{version}/%{pypi_name}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-rpm-macros
BuildRequires:  unzip

BuildRequires:  python%{python3_pkgversion}-flit-core
BuildRequires:  python%{python3_pkgversion}-pip 

# Upstream uses nox for testing.
%if %{with tests}
BuildRequires:  python%{python3_pkgversion}-pytest
BuildRequires:  python%{python3_pkgversion}-pretend
%endif

%global _description %{expand:
python-packaging provides core utilities for Python packages like utilities for
dealing with versions, specifiers, markers etc.}

%description %_description

%prep
%autosetup -p1 -n %{pypi_name}-%{version}

%build
%{python3} -m flit_core.wheel

%install
%py3_install_wheel %{pypi_name}-%{version}-py3-none-any.whl

%if %{with tests}
%check
%pytest
%endif


%files -n python%{python3_pkgversion}-%{pypi_name}
%license LICENSE LICENSE.APACHE LICENSE.BSD
%doc README.rst CHANGELOG.rst CONTRIBUTING.rst
%{python3_sitelib}/%{pypi_name}/
%{python3_sitelib}/%{pypi_name}-*-info/


%changelog
* Thu Oct 12 2023 Tomáš Hrnčiar <thrnciar@redhat.com> - 23.2-1
- Initial package
- Fedora contributions by:
      Charalampos Stratakis <cstratak@redhat.com>
      Iryna Shcherbina <shcherbina.iryna@gmail.com>
      Jeroen van Meeuwen (Kolab Systems) <vanmeeuwen@kolabsys.com>
      Karolina Surma <ksurma@redhat.com>
      Lumir Balhar <lbalhar@redhat.com>
      Miro Hrončok <miro@hroncok.cz>
      Tomáš Hrnčiar <thrnciar@redhat.com>
      Yaakov Selkowitz <yselkowi@redhat.com>
      Zbigniew Jędrzejewski-Szmek <zbyszek@in.waw.pl>

